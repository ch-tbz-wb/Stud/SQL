## Zugriff auf DB über Python

### Voraussetzung

Um eine Verbindung aus Python heraus zu einer Datenbank vornehmen zu können, muss einmalig ein MySQL-Connector für Python installiert werden. Siehe dazu [MySQL Connector](https://dev.mysql.com/doc/connector-python/en/).

### Ablauf

Ein Python-Skript mit Zugriff auf eine Datenbank ist grundsätzlich wie folgt aufgebaut:

```python

import mysql.connector

# Verbindungsinformationen
config = {
    'user': '<user>',  # Mit MySQL-Benutzernamen ersetzen
    'password': '<passwort>',  # Passwort ersetzen
    'host': '<Server>',         # IP-Adresse/Hostname des DB-Servers
    'port': '<port>',     # Port-Nummer angeben (Default, falls nicht angegeben: 3306)
    'database': '<dbname>',  # Namen der Datenbank angeben
    'raise_on_warnings': True
}

try:
    # Verbindung zur Datenbank herstellen mit obigen Vorgaben
    conn = mysql.connector.connect(**config)

    # Cursor erstellen
    cursor = conn.cursor()

    # SQL-Anfrage ausführen mittels Cursor
    cursor.execute(f"SELECT * FROM mysql.TABLES")

    for row in cursor.fetchall():
        print(row)

except mysql.connector.Error as err:
    print(f"Fehler: {err}")

finally:
    # Verbindung schließen
    if conn.is_connected():
        cursor.close()
        conn.close()
        print("Verbindung zur Datenbank wurde geschlossen.")

```

### Beispiel-Skripts

* [Daten auslesen aus Tabelle](DBAccess_ListTables.py)
* [Datenbank erstellen](DBAccess_CreateDB.py)

### Aufträge

* [Projektgruppen zusammenstellen](Auftrag_Projektgruppen/Auftrag_Projektgruppen_bilden.md)