## Auftrag: Projektgruppen zusammenstellen

Im Folgenden soll ein Python-Skript (Alternative: Stored Procedure) erstellt werden.

### Voraussetzung

Erstelle eine Datenbank mit den Statements aus [Projektgruppen.sql](Projektgruppen.sql).

### Einleitung und Anforderungen

In der Tabelle “Teilnehmende” sind Personen aus verschiedenen Klassen (bspw. “PE22a”) und Stufen (Lehrjahr) aufgeführt. In der Tabelle “Lehrpersonen” sind alle Lehrpersonen definiert.

Die Personen sollen für einen Sport-Event möglichst zufällig zu Gruppen zusammengestellt werden.

Die Aufteilung soll folgenden Anforderungen entsprechen:

#### Muss-Kriterien

* Jede Lehrperson soll eine Gruppe übernehmen. Die Anzahl Gruppen entspricht also der Anzahl Lehrpersonen.
* In einer Gruppe sollen Personen aus allen Stufen vertreten sein.
* Die resultierenden Gruppen sollten ungefähr gleich gross sein

#### Kann-Kriterien

* Eine teilnehmende Person soll immer mit mindestens einer anderen Person ihrer Klasse zusammen in der Gruppe sein. Sie kennt also mindestens eine Person gut.
* Von jeder Stufe sollten in einer Gruppe möglichst gleichviele Personen vertreten sein 

Der Einfachheit halber können die vergebenen Gruppennummern den Klassen-IDs entsprechen.

### Auftrag

Erstelle ein Skript (Python), welches die Tabelle “zuteilung” füllt (welche Teilnehmenden in welche Gruppen zugeteilt wurden).  