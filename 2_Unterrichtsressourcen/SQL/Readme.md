# SQL - Structured Query Language

[[_TOC_]]

Die Sprache, mit welcher Befehle dem DBMS mitgeteilt werden, heisst SQL (Structured Query Language). Diese Sprache gilt als Standard bei relationalen Datenbanken. In diesem Teil geht es darum, diese Sprache kennenzulernen.

## Einführung

SQL orientiert sich stark an der englischen Sprache und ist deshalb einfach lesbar. SQL ist beschreibend ("deskriptiv") und beschreibt nur das gewünschte **Resultat**, nicht aber die nötigen Rechenschritte.

Im Folgenden werden einige grundlegende Sprachelemente zum Einfügen, Verändern, Abfragen und Löschen von Daten gezeigt. Danach werden Sprachelemente beschrieben, mit welchen Daten zusammengefasst werden können. Schlussendlich werden Transaktionen angesprochen. Mittels Transaktionen stellen die DBMS sicher, dass eine Reihe von Datenbankoperationen als eine einzige, unteilbare Einheit behandelt wird.

### SQL-Terminologie

* **SQL (Structured Query Language)**: Eine standardisierte Sprache zur Abfrage und Manipulation von relationalen Datenbanken.
* **SELECT**: Ein SQL-Befehl, der verwendet wird, um Daten aus einer Datenbank abzurufen.
* **INSERT**: Ein Befehl, der zum Einfügen von Daten in eine Tabelle verwendet wird.
* **UPDATE**: Ein Befehl, der zum Ändern bestehender Datensätze in einer Tabelle verwendet wird.
* **DELETE**: Ein Befehl, der zum Löschen von Datensätzen aus einer Tabelle verwendet wird.
* **JOIN**: Eine Operation, die verwendet wird, um Daten aus zwei oder mehr Tabellen basierend auf einer Beziehung zwischen ihnen zu kombinieren.
* **UNION**: Eine Operation, die verwendet wird, um Daten aus Tabellen zusammenzufügen.
* **CREATE**: Eine Operation, um Datenbanken und Tabellen zu erstellen
* **ALTER**: Eine Operation, um Tabellen und Spalten zu ändern
* **DROP**: Löscht Datenbanken oder Tabellen

### Datenintegrität und Transaktionen

* **Constraints**: Regeln, die auf Datenspalten in einer Tabelle angewendet werden, um die Genauigkeit und Zuverlässigkeit der in der Datenbank gespeicherten Daten zu gewährleisten (bspw. "NOT NULL Constraint")
* **Datenintegrität**: Die Genauigkeit, Vollständigkeit und Konsistenz von Daten in einer Datenbank.
* **Referenzielle Integrität**: Eine Eigenschaft, die sicherstellt, dass Beziehungen zwischen Tabellen durch Fremdschlüssel erhalten bleiben.
* **Transaktion**: Eine Reihe von SQL-Operationen, die als einzelne Arbeitseinheit behandelt werden. Transaktionen helfen, die Datenintegrität zu gewährleisten. 



## DDL (Data Defintion Language)

Bei der DDL geht es darum die Tabellen und andere Objekte zu erstellen und zu verändern. Wir haben in der Modellierung gesehen, dass die Verbindungen zwischen Tabellen/Entitäten mit Foreign-Keys erstellt werden. Wenn wir nur die Attribute erstellen, weiss das DBMS nicht, dass eine Verbindung existiert. Die Verbindung zwischen zwei Attributen muss explizit auch erstellt werden.

Schauen wir uns als Beispiel die beiden markierten Tabellen an. Die Verknüpfung deren beiden wird dem DBMS explizit (mit der Anweisung *CONSTRAINT* ) mitgeteilt.

![constraint](./ddl/constraint.png)



```sql
CREATE TABLE IF NOT EXISTS `mydb`.`Abteilung` (
  `AbteilungId` INT NOT NULL,
  `Name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`AbteilungId`));

CREATE TABLE IF NOT EXISTS `mydb`.`Mitarbeiter` (
  `MitarbeiterId` INT NOT NULL,
  `Name` VARCHAR(45) NOT NULL,
  `AbteilungId` INT NOT NULL,
  PRIMARY KEY (`MitarbeiterId`),
  CONSTRAINT `fk_Mitarbeiter_Abteilung`
    FOREIGN KEY (`AbteilungId`)
    REFERENCES `mydb`.`Abteilung` (`AbteilungId`);
```



Es gibt viele gute Tutorials im Internet, um die Sprache zu lernen. Sinnvollerweise werden diese Tutorials zusammen mit einer Aufgabe bearbeitet. Folgend findet Ihr Links auf Tutorials. Schaut euch die Links zusammen mit den Aufgaben an.

[Tutorial w3schools]

[Tutorial Geeks for Geeks]

[Aufgaben](./Aufträge_DDL.md)



## DML (Data Manipulation Language)

Bei der DMS geht es darum wie man Daten in die Datenbank schreibt, bearbeitet und wieder ausliest. Die einfachen Befehle sind schnell gelernt, aber die erweiterten Konzepte werden dann komplizierter. 

### Basis Befehle

Die SQL-Befehle, welche am meisten verwendet werden, sind: SELECT, INSERT, UPDATE, DELETE. Mit diesen Befehlen lassen sich Daten abfragen, neu einfügen, ändern oder löschen.

[Einführung in SQL]

[Tutorial w3schools]

[Tutorial Geeks for Geeks]

[Aufgaben](./Aufträge_DML_1.md)



### Fortgeschrittene SQL-Konzepte

Die fortgeschrittenen Konzepte in der Übersicht:

**Aggregationen**

Aggregationen dienen dazu Daten zusammenzufassen - also zu rechnen. Relevante Links:

[COUNT](https://www.w3schools.com/sql/sql_count.asp) | [Min / Max](https://www.w3schools.com/sql/sql_min_max.asp) | [SUM](https://www.w3schools.com/sql/sql_sum.asp) | [AVG](https://www.w3schools.com/sql/sql_avg.asp)

**Gruppierung von Inhalten (GROUP BY) und Aggregation**

Gruppiert Daten nach spezifischen Spalten. Sobald man gruppiert, kommen auch Aggregationen ins Spiel. Relevante Links:

[GROUP BY](https://www.w3schools.com/sql/sql_groupby.asp) | [HAVING](https://www.w3schools.com/sql/sql_having.asp)

**Tabellenverknüpfungen (JOIN)**

Wenn Daten von mehreren Tabellen benötigt werden, können diese zusammengebracht werden. Um Joins und Unions besser zu verstehen, ist die [Mengelehre](https://gitlab.com/ch-tbz-it/Stud/m164/-/tree/main/4.Tag?ref_type=heads#mengenlehre) hilfreich.

Relevante Links:

[INNER JOIN](https://www.w3schools.com/sql/sql_join_inner.asp) | [LEFT JOIN](https://www.w3schools.com/sql/sql_join_left.asp) | [RIGHT JOIN](https://www.w3schools.com/sql/sql_join_right.asp)

**Kombinieren von Ergebnismengen (UNION)**

Union vereint zwei separate Abfragen in eine Abfrage. Dies geht nur, wenn die gleichen Spalten vorhanden sind in beiden Resultaten. Relevante Links:

[UNION](https://www.w3schools.com/sql/sql_union.asp)

**Unterabfragen**

Unterabfragen sind Abfragen innerhalb anderer Abfragen. Relevante Links:

[IN](https://www.w3schools.com/sql/sql_in.asp)

Unterabfragen können nicht nur im WHERE-Teil vorkommen, sondern auch im SELECT, HAVING, JOIN und anderen. Der WHERE-Teil ist sicherlich der wichtigste. Beispiele

``` sql
-- im WHERE
SELECT ColumnName
FROM TableName
WHERE ColumnName IN (SELECT ColumnName FROM AnotherTable);

-- im FROM
SELECT temp.ColumnName
FROM (SELECT ColumnName FROM TableName) AS temp;

-- im SELECT
SELECT ColumnName, (SELECT COUNT(*) FROM AnotherTable) AS Count
FROM TableName;

-- im HAVING
SELECT ColumnName, COUNT(*)
FROM TableName
GROUP BY ColumnName
HAVING COUNT(*) > (SELECT COUNT(*) FROM AnotherTable);

-- im JOIN
SELECT a.ColumnName, b.ColumnName
FROM TableName a
JOIN (SELECT ColumnName FROM AnotherTable WHERE Condition = True) b ON a.ColumnName = b.ColumnName;
```



[Aufgaben](./Aufträge_DML_2.md)



## Datenbanktransaktionen

Transaktionen in Datenbanksystemen dienen dazu, die Integrität und Konsistenz der Daten zu gewährleisten, besonders in Umgebungen, in denen mehrere Operationen gleichzeitig auf dieselben Daten zugreifen.

Die zugehörigen SQL-Befehle werden zusammengefasst unter dem Kürzel "TCL", der Abkürzung für "Transaction Control Language".

 [Einführung in Datenbank-Transaktionen](Transaktionen.md).

Es ist wichtig, dass du das Konzept für Transaktionen verstanden hast. 

**Einen wichtigen Praxistipp gibt es**:  Schreibe nie ein INSERT, UPDATE oder DELETE Statement ohne Transaktion! Es ist eine Frage der Zeit, bis du einen Fehler machst, und Daten löscht, veränderst.




[Einführung in SQL]: https://www.youtube.com/watch?v=yU1Ek8SKiOQ
[Datenbanktransaktionen]: https://www.youtube.com/watch?v=fZWE7l6IVl8
[Tutorial w3schools]: https://www.w3schools.com/sql/
[Tutorial Geeks for Geeks]: https://www.geeksforgeeks.org/sql-tutorial/