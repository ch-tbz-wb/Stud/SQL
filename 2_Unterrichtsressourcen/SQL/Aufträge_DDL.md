# DDL - Assignments

[[_TOC_]]



## Auftrag 1

Bildet nun euer physisches Datenmodell mit [Forward Engineering](https://dev.mysql.com/doc/workbench/en/wb-forward-engineering.html) in die Datenbank ab. Bevor ihr nun die Befehle ausführt, die euch angezeigt werdet, schaut euch alle genau an. Die Befehle, die ihr nicht versteht sollt ihr im Tutorial nachlesen. 

Speichert das komplette Skript als SQL-Skript-Datei (mit Dateiendung .sql). So habt ihr das Skript und könnt es einfach wieder ausführen später.

Ignoriert die SET-Anweisungen am Schluss und am Ende. Alle anderen Anweisungen solltet ihr aber grundsätzlich verstanden haben.

PS: Auch der umgekehrte Weg (Reverse Engineering) geht. Dabei erstellt ihr ein Diagramm aufgrund einer existierenden Datenbank.

## Auftrag 2

Für eine Tabelle erstelle eine Anweisung, welches eine zusätzliche Spalte zu einer Tabelle hinzufügt.

Für die hinzugefügte Spalte, erstelle eine Anweisung, welches den Datentypen ändert.

Erstelle nun eine Anweisung welches die hinzugefügt Spalte wieder löscht.

Speichere die Anweisungen in einem SQL-Skript (.sql-Datei).

## Weitere Aufträge

Grundsätzlich könnt ihr davon ausgehen, dass ihr die Befehle zum erstellen von Datenbanken, Tabellen und Spalten generieren könnt. Darum gehen wir nicht tiefer auf die Befehle ein.

Ihr könnt aber jederzeit andere Datenbanken, z.B. aus den Übungen oder von euren KollegInnen zusätzlich erstellen um den Ablauf zu üben

