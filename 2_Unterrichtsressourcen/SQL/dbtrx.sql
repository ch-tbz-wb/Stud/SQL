DROP DATABASE IF EXISTS `dbtrx`;
CREATE DATABASE `dbtrx`;
USE `dbtrx`;


-- Mitarbeiter-Tabelle erstellen
DROP TABLE IF EXISTS Mitarbeiter;
CREATE TABLE Mitarbeiter (
    MitarbeiterID INT PRIMARY KEY,
    Vorname VARCHAR(255),
    Nachname VARCHAR(255),
    Gehalt DECIMAL(10, 2),
    AbteilungID INT
);

-- Daten in die Mitarbeiter-Tabelle einfügen
INSERT INTO Mitarbeiter (MitarbeiterID, Vorname, Nachname, Gehalt, AbteilungID)
VALUES
(1, 'Max', 'Mustermann', 5000, 1),
(2, 'Anna', 'Musterfrau', 5500, 1),
(3, 'John', 'Doe', 4800, 2),
(4, 'Emily', 'Smith', 5200, 2),
(5, 'Michael', 'Johnson', 5300, 3);

-- Kunden-Tabelle erstellen
DROP TABLE IF EXISTS Kunden;
CREATE TABLE Kunden (
    KundenID INT PRIMARY KEY,
    Vorname VARCHAR(255),
    Nachname VARCHAR(255),
    ZustaendigMitarbeiterID INT
);

-- Daten in die Kunden-Tabelle einfügen
INSERT INTO Kunden (KundenID, Vorname, Nachname, ZustaendigMitarbeiterID)
VALUES
(1, 'Peter', 'Schmidt', 3),
(2, 'Lisa', 'Muller', 3),
(3, 'Markus', 'Schulz', 4),
(4, 'Sandra', 'Wagner', 5);

-- Bestellungen-Tabelle erstellen
DROP TABLE IF EXISTS Bestellungen;
CREATE TABLE Bestellungen (
    BestellungenID INT PRIMARY KEY,
    KundenID INT,
    ProduktID INT,
    Menge INT
);

-- Daten in die Bestellungen-Tabelle einfügen
INSERT INTO Bestellungen (BestellungenID, KundenID, ProduktID, Menge)
VALUES
(1, 1, 101, 2),
(2, 1, 102, 1),
(3, 2, 103, 3),
(4, 3, 101, 1),
(5, 3, 104, 2),
(6, 4, 105, 4),
(7, 4, 106, 3),
(8, 4, 107, 2);

-- Produkte-Tabelle erstellen
DROP TABLE IF EXISTS produkte;
CREATE TABLE if not EXISTS Produkte (
    ProduktID INT PRIMARY KEY,
    Name VARCHAR(255),
    Kategorie VARCHAR(255),
    Preis DECIMAL(10, 2)
);

-- Daten in die Produkte-Tabelle einfügen
INSERT INTO Produkte (ProduktID, Name, Kategorie, Preis)
VALUES
(101, 'Laptop', 'Elektronik', 1200),
(102, 'Smartphone', 'Elektronik', 800),
(103, 'The SQL Book', 'Buecher', 30),
(104, 'Kaffeemaschine', 'Kueche', 100),
(105, 'Fahrrad', 'Sport', 500),
(106, 'Sandwich', 'Esswaren', 8.5),
(107, 'Teigwaren', 'Esswaren', 4.5);