# DML 2 - Assignments

[[_TOC_]]

Die folgenden Abfragen sind Vorschläge, um die verschiedenen Funktionen zu testen. **Erkläre jeweils in einem kurzen Satz was du auslesen möchtest** (Kommentiere deine Querys).



**Aggregationen**

- Test die verschiedenen Aggregationen



**Group BY**

- Erstelle ein einfache Gruppierung von Daten
- Erstelle eine Gruppierung über mehrere Spalten
- Teste die verschiedenen Aggregationen zusammen mit der Gruppierung
- Teste die ***HAVING***-Anweisung nach der Gruppierung. 
- Teste sowohl die ***WHERE***-Anweisung vor der Gruppierung und dann die ***HAVING***-Anweisung nach der Gruppierung.
- Teste wie du auf die Resultate der Aggregation filtern kannst in der ***HAVING***-Anweisung.



**Join**

Für den ***JOIN*** über zwei Tabellen stelle sicher, dass du **nicht** alle Daten der One-Beziehung in der Many-Beziehungsseite verwendest. 

- Erstelle einen ***INNER JOIN*** über die beiden Tabellen
- Erstelle einen ***LEFT JOIN*** über die beiden Tabellen
- Erstelle einen ***RIGHT JOIN*** über die beiden Tabellen. Ist das das gleiche wie ein LEFT JOIN, wenn man die Reihenfolge der Tabellen ändert?
- Verwende zusätzliche Filterungen in der ***WHERE***-Klausel
- Verwende ***AND*** im ***JOIN***

Stelle sicher, dass du korrekte Daten hast zum Testen für den Join über drei Tabellen. Verwende zwei Tabellen, die mit einer Transformationstabelle verbunden sind. Verwende in der Transformationstabelle nur ein Teil der beiden anderen Tabellen. Die beiden Tabellen sollen also Einträge haben, die in der Transformationstabelle nicht vorkommen!

- Teste die verschiedenen Kombinationen mit ***INNER***, ***LEFT*** und ***RIGHT JOIN*** über drei Tabellen



**Union**

Teste die ***UNION***-Anweisung. Stelle sicher, dass deine beiden Resultate vor dem ***UNION*** die gleichen Spalten haben. Du kannst dies testen, indem du zwei gefilterte Abfragen  über die gleiche Tabelle erstellst.



**Unterabfragen**

Für Unterabfragen führe dir vor Augen, dass du immer ein Datensatz hast und all die gelernten Anweisungen auf Datensätzen basieren. Ob du nun also einen Join über eine Tabelle machst oder über eine Abfrage ist genau das gleiche - nämlich ein Datensatz.

Teste hier vor allem die ***IN***-Anweisung, die sicherlich am Meisten vorkommt. Du kannst gerne auch andere Bereiche testen



**Weitere Abfragen**

Die Grenzen von SQL sind noch lange nicht ausgetestet. Solche Konstrukte sind erlaubt:

```sql
select field2 as grpvalue, nr, summe from (
	select field2, count(*) as nr, sum(t1.field1) as summe from (
		select t1.field1, t1. field2, field3 from anytable as t1
	    inner join (select field3 from anothertable where field4 like '%whatever') as t2 
           on t1.key1 = t2.key2
	)
	group by field2
)
order by nr
```

Versuche selbstständig weitere eigene Abfragen in deinem Themenbereich zu erstellen.

