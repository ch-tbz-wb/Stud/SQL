# Datenbanktransaktionen

[[_TOC_]]

## Begriffe

* **Datenintegrität**: Die Genauigkeit, Vollständigkeit und Konsistenz von Daten in einer Datenbank.
* **Transaktion**: Eine Reihe von SQL-Operationen, die als einzelne Arbeitseinheit behandelt werden. Transaktionen helfen, die Datenintegrität zu gewährleisten.
* **ROLLBACK**: Das TCL-Command, mit welchem alle während der Transaktion erfolgten Änderungen zurückgesetzt werden.
* **COMMIT**: Das TCL-Command, mit welchem die während der Transaktion erfolgten Änderungen dauerhaft übernommen werden.
* **LOCK**: Eine Sperre auf einer Ressource, welche Aktionen durch andere Benutzer verhindert.
* **Deadlock**: Situation, wenn Transaktion A ewig auf die Freigabe einer Sperre durch Transaktion B wartet, während umgekehrt Transaktion B auf die Freigabe einer Sperre durch Transaktion A wartet. Die DBMS gehen unterschiedlich mit dieser Problematik um.
* **Timeout**: Wenn eine Transaktion zu lange auf die Freigabe einer Sperre (Lock durch ein andere Transaktion) warten muss, führt dies zu einem "Timeout"-Fehler. Das Statement wurde in diesem Falle durch das DBMS abgebrochen.
* **IsolationLevel**: Definiert, wie die einzelnen Transaktionen voneinander abgeschirmt sind.

## Problemstellungen und Grundkonzept

Mittels Transaktionen stellt ein DBMS sicher, dass Datenbankoperationen als Einheit durchgeführt wird. Nach dem Motto: "Alles oder nichts". Das Konzept dahinter heisst "ACID".

### ACID

ACID steht für **Atomicity**, **Consistency**, **Isolation**, und **Durability**. Es ist ein Konzept, das die grundlegenden Eigenschaften beschreibt, die Transaktionen in (v.a. relationalen) Datenbanksystemen erfüllen müssen, um Datenintegrität und fehlerfreie Verarbeitung zu gewährleisten. Diese vier Eigenschaften werden nachfolgend beschrieben:

* **Atomicity (Atomarität)**: Eine Transaktion wird als unteilbare Einheit betrachtet, die entweder vollständig ausgeführt oder vollständig abgebrochen wird. Wenn also ein Teil der Transaktion fehlschlägt, wird die gesamte Transaktion zurückgesetzt, als wäre sie nie passiert. Dies verhindert teilweise durchgeführte Operationen, die Daten in einem inkonsistenten Zustand hinterlassen könnten.

* **Consistency (Konsistenz)**: Transaktionen müssen die Datenbank von einem konsistenten Zustand in einen anderen überführen. Das bedeutet, dass alle Geschäftsregeln und Integritätsbedingungen vor und nach der Transaktion erfüllt sein müssen. Wenn eine Transaktion die Datenbank in einen inkonsistenten Zustand überführen würde, wird sie nicht durchgeführt.

* **Isolation (Isolierung)**: Änderungen, die innerhalb einer Transaktion gemacht werden, sind isoliert von anderen gleichzeitig ablaufenden Transaktionen, bis sie abgeschlossen sind. Dies verhindert, dass Transaktionen, die parallel ausgeführt werden, sich gegenseitig beeinflussen und möglicherweise zu Dateninkonsistenzen führen.

* **Durability (Dauerhaftigkeit)**: Sobald eine Transaktion erfolgreich abgeschlossen wurde, sind die Änderungen dauerhaft in der Datenbank gespeichert, auch im Falle eines Systemausfalls. Dies garantiert, dass die Ergebnisse der Transaktion nicht verloren gehen, selbst wenn direkt nach der Transaktion ein Problem auftritt.

### Use Cases und Hintergrund

Wie RDBMS es ermöglichen, dass Datenbankoperationen als Einheit durchgeführt oder verworfen wird, erfährst du im Video [Datenbanktransaktionen].

### Mögliche Problemfälle durch gleichzeitigen Zugriff

Der gleichzeitige Zugriff auf dieselben Daten durch mehrere Nutzer kann zu folgenden Problemfällen führen:

1) **Dirty Reads**
Ein "Dirty Read" tritt auf, wenn eine Transaktion Daten liest, die von einer anderen Transaktion geändert wurden, die noch nicht abgeschlossen (committed) ist.
2) **Non-Repeatable Reads**
Ein "Non-Repeatable Read" tritt auf, wenn eine Transaktion dieselben Daten mehrmals liest und unterschiedliche Werte erhält, weil eine andere Transaktion die Daten zwischen den Lesevorgängen geändert und abgeschlossen hat. 
4) **Phantom Reads**
Ein "Phantom Read" tritt auf, wenn eine Transaktion eine Abfrage wiederholt und dabei feststellt, dass sich die Anzahl oder Existenz von Datensätzen verändert hat, weil eine andere Transaktion neue Datensätze hinzugefügt oder existierende gelöscht hat.

Als Lösung zur Vorbeugung gibt es bei den DBMS das Konzept der "IsolationLevels". Damit wird definiert, wie die Transaktionen voneinander abgeschirmt werden.

Hier ist eine Tabelle, die die drei Problemfälle (Dirty Reads, Non-Repeatable Reads, Phantom Reads) gegenüberstellt und angibt, welche Isolationsstufen in SQL-Datenbanken diese Probleme lösen:

| Problemfall          | Beschreibung                                                                                                                                                                                                 | Isolationsstufe, die das Problem löst         |
| -------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | --------------------------------------------- |
| Dirty Reads          | Das Lesen von Daten, die von einer anderen Transaktion geändert, aber noch nicht festgeschrieben wurden.                                                                                                     | Read Committed, Repeatable Read, Serializable |
| Non-Repeatable Reads | Eine Transaktion liest dieselben Daten mehrmals und erhält verschiedene Werte, weil andere Transaktionen die Daten zwischen den Lesevorgängen ändern und festgeschrieben haben.                              | Repeatable Read, Serializable                 |
| Phantom Reads        | Eine Transaktion stellt fest, dass sich die Anzahl oder Existenz von Datensätzen zwischen zwei Abfragen verändert hat, weil andere Transaktionen neue Datensätze hinzugefügt oder vorhandene gelöscht haben. | Serializable                                  |


Die Isolationsstufen, die im SQL-Standard definiert sind und wie sie sich auf diese Problemfälle auswirken, sind:

**Read Uncommitted**: Die niedrigste Isolationsstufe, bei der Dirty Reads möglich sind, aber Non-Repeatable Reads und Phantom Reads nicht verhindert werden.
**Read Committed**: Verhindert Dirty Reads, aber Non-Repeatable Reads und Phantom Reads können immer noch auftreten.
**Repeatable Read**: Verhindert Dirty Reads und Non-Repeatable Reads, aber Phantom Reads können auftreten.
**Serializable**: Die strengste Isolationsstufe, die alle drei Probleme (Dirty Reads, Non-Repeatable Reads, Phantom Reads) löst, indem sie eine vollständige Isolation der Transaktionen gewährleistet.

MySQL unterstützt alle vier Isolationlevels. Das Setzen erfolgt über das Statement 

``` sql
SET TRANSACTION ISOLATION LEVEL REPEATABLE READ; 
-- [REPEATABLE READ|READ COMMITTED|READ UNCOMMITTED|SERIALIZABLE] (Default: REPEATABLE READ)
```

&rarr; [MySQL Isolation Levels](https://dev.mysql.com/doc/refman/8.0/en/innodb-transaction-isolation-levels.html#isolevel_repeatable-read)
&rarr; Details siehe Video [Transaction Isolation Levels](https://www.youtube.com/watch?v=CTCAo89fcQw) 5'11''


## Übung mit Transaktionen

### Grundsätzlicher Ablauf einer Transaktion

Eine Transaktion wird eingeleitet mit dem SQL-Command "Start Transaction". Alle darauf folgenden SQL-Statements laufen nun in einer Art "geschützten" Umgebung ab.
Erst nach einem "Commit" werden die Änderungen dauerhaft ins System übernommen, bzw. bei einem Rollback allesamt verworfen.

**Voraussetzung** für **alle** folgenden Übungen**:
``` sql
SET Autocommit = FALSE  -- Schaltet den Commit nach jedem Statement aus
SET TRANSACTION ISOLATION LEVEL REPEATABLE READ; -- Setzt den IsolationLevel auf "Repeatable Read"
```


**Voraussetzung** für die folgende Übung:

* Führe das SQL-Skript [dbtrx.sql](dbtrx.sql) aus. Die Datenbank "dbtrx" wird damit erstellt oder neu bestückt (zugehöriges [ERD](../media/ERD_dbtrx.jpg))

``` sql
START TRANSACTION; -- Start: Alle nachfolgenden Statements bis COMMIT oder ROLLBACK werden als Einheit behandelt (alle übernommen oder aber alle verworfen)

-- Nachfolgend werden diverse Änderungen vorgenommen
UPDATE Produkte SET Preis = Preis + 0.35;
UPDATE Mitarbeiter SET Gehalt = Gehalt * 2;
INSERT into Produkte (ProduktID, Name, Kategorie, Preis) Values (200, "Reis", "Esswaren", 3.5);
DELETE FROM Bestellungen where Bestellungen where Menge > 1;

COMMIT; -- um die Änderungen dauerhauft zu übernehmen
-- *** ODER ***
ROLLBACK; -- um die Änderungen zu verwerfen
```

### Zurücksetzen von vorgenommenen Änderungen (Undo)

Mittels einer Transaktion ist es möglich, vorgenommene Änderungen rückgängig zu machen. Am Ende einer Transaktion kann entschieden werden, ob alle Änderungen übernommen (mittels "Commit") oder allesamt verworfen werden sollen (mittels "Rollback").

_Damit die Resultate der folgenden Statements betrachtet werden können, sollten diese einzeln durchgeführt werden._

**Voraussetzung für die Übung**:

* Führe das SQL-Skript [dbtrx.sql](dbtrx.sql) aus. Die Datenbank "dbtrx" wird damit erstellt oder neu bestückt.

``` sql
use dbtrx;
Start Transaction; 

-- Aktuelle Daten der Mitarbeitenden werden gelesen
SELECT MitarbeiterID, Gehalt from Mitarbeiter;

-- Daten aller Mitarbeiter werden geändert
UPDATE Mitarbeiter SET Gehalt = Gehalt + 555;

-- Wir schauen, wie die Daten aussehen: Sie wurden geändert!
SELECT MitarbeiterID, Gehalt from Mitarbeiter;

ROLLBACK; -- Transaktion wird storniert. Vorgenommene Änderungen werden zurückgesetzt.

-- Die Daten haben wieder den Stand *vor* der Transaktion
SELECT MitarbeiterID, Gehalt from Mitarbeiter;


```

### Zugriff auf Daten regeln

**Voraussetzung für alle Übungen**:

* Führe das SQL-Skript [dbtrx.sql](dbtrx.sql) aus. Die Datenbank "dbtrx" wird damit erstellt oder neu bestückt.
* Starte _zwei_ SQL-Editor-Instanzen.
_Im Folgenden werden Eingaben in den ersten Editor durch "E1", bzw. Editor zwei durch E2 gekennzeichnet._

**E1**

``` sql
use dbtrx;
Start Transaction; -- Damit startet E1 eine Transaktion
SELECT * 
FROM Mitarbeiter
WHERE MitarbeiterID = 1
FOR UPDATE; -- erreicht, dass dieses Resultset für andere Änderungen gesperrt wird
```

**E2**

``` sql
use dbtrx;
Start Transaction; -- Damit startet E2 eine Transaktion

-- Der folgende SQL funktioniert bestens.
SELECT * FROM Mitarbeiter WHERE MitarbeiterID = 2 FOR UPDATE;

-- Die Daten von Mitarbeiter 2 können problemlos geändert werden
UPDATE Mitarbeiter SET Gehalt = Gehalt + 200 WHERE MitarbeiterID = 2;

-- Mitarbeiter 1 kann abgefragt werden
SELECT * FROM Mitarbeiter WHERE MitarbeiterID = 1;
-- Wenn aber ein Update erfolgen soll (Lock wird gewünscht) ergibt das einen TIMEOUT-Fehler
SELECT * FROM Mitarbeiter WHERE MitarbeiterID = 1 FOR UPDATE;
-- Natürlich dasselbe, wenn ein Update erfolgen soll (Lock wird dann benötigt) ==> TIMEOUT
UPDATE Mitarbeiter SET Gehalt = Gehalt + 350 WHERE MitarbeiterID = 1;

-- Allerdings beendet der Fehler die Transaktion nicht!  
COMMIT; -- Erst mit einem Commit werden die vorgenommenen Änderungen dauerhaft übernommen und die Locks freigegeben.
```

Das DBMS meldet nach einiger Zeit einen Timeout. Der Grund liegt darin, dass die Transaktion in E1 den Datensatz für Mitarbeiter 1 gesperrt hat (Lock).

**E1**

``` sql
COMMIT; -- Beendet die Transaktion, übernimmt alle gemachten Änderungen dauerhaft und gibt Locks frei
```

**E2**

``` sql
-- nun können wieder Locks auf Mitarbeiter 1 gesetzt und Änderungen vorgenommen werden
SELECT * FROM Mitarbeiter WHERE MitarbeiterID = 1 FOR UPDATE;
UPDATE Mitarbeiter SET Gehalt = 5200 WHERE MitarbeiterID = 1;

ROLLBACK; -- Mit dem Rollback verwerfen wir diese Änderung und geben die Locks frei.
```

## Angaben über laufende Transaktionen

Bei Problemen mit Deadlocks sind Informationen über die laufenden Transaktionen (Wer blockiert wen) hilfreich. Diese Angaben können in MySQL über folgende Tabellen eruiert werden:

* information_schema.**INNODB_TRX**
* performance_schema.**data_locks**
* performance_schema.**data_lock_waits**

oder noch einfacher über die View **sys.innodb_lock_waits**

## Links

* Prof. Drumm, HF Aachen ([Playlist](https://www.youtube.com/playlist?list=PLl09U8aTDcv1eIkxyPKNAKKmqPJR3RC0o))
  * [Datenbanktransaktionen] 15'11''
* MySQL InnoDB [Transaction & Locking Information](https://dev.mysql.com/doc/refman/8.0/en/innodb-information-schema-transactions.html)
* MySQL InnoDB [IsolationLevels](https://dev.mysql.com/doc/refman/8.0/en/innodb-transaction-isolation-levels.html)

[Datenbanktransaktionen]: https://www.youtube.com/watch?v=fZWE7l6IVl8