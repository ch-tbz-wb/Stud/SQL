# DML 1 - Assignments

[[_TOC_]]



## Auftrag 1 - Daten einfügen

Da ihr nun eine Datenbank habt, sollt ihr diese natürlich mit Daten füllen. Erstellt eine SQL-Skript-Datei mit ***INSERT***-Statements.  Erstellt eine sinnvolle Anzahl an Einträgen (mindestes 3-10) pro Tabelle, da ihr später auch Abfragen machen sollt.  Haltet folgende Regeln ein:

1. Stellt bei mindestens einer Tabelle die Funktion *Autoinkrement* beim Primärschlüssel ein. Dies kann bei allen Tabellen eingeschaltet werden (ausser bei Transformationstabellen). Ihr werdet sehen, dass ihr dann den Wert für den Primärschlüssel **nicht** setzen müsst und der automatisch vergeben wird.

2. Mindestens eine Tabelle soll mit einem INSERT-Statement gefüllt werden - also mehrere Zeilen Inhalt mit einer INSERT-Anweisung. Abhängig vom DBMS kann es aber sein, dass dies nicht geht. Die Standard-Datenbanken unterstützen diese Funktion

   

Natürlich darfst du auch andere / weitere Statements schreiben



## Auftrag 2 - Daten lesen

Lies nun die Daten aus den Tabellen mit mit dem ***SELECT***-Befehl und erfülle folgenden Bedingungen:

1. Lies eine Tabelle als gesamtes aus (ohne WHERE) und zeige alle Spalten (*-Operator)
2. Filtere mind.eine Tabelle und verwende den Vergleichsoperator ***=***
3. Filtere mind. eine Tabelle und verwende die ***OR***-Anweisung
4. Filtere mind.eine Tabelle und verwende die ***UND***-Anweisung
5. Filtere mind. eine Tabelle und verwende den ***LIKE***-Anweisung
6. Filtere mind. eine Tabelle nach einem Datum. Verwende ***<=*** und/oder ***>=*** und ***BETWEEN***
7. Sortiere die Resultate mit ***ORDER BY***, einmal aufsteigend und einmal absteigend



Natürlich darfst du auch andere / weitere Statements schreiben



## Auftrag 3 - Daten aktualisieren

Hier werden wir Daten aktualisieren mit Hilfe des ***UPDATE***-Befehls. Bedingungen:

1. Aktualisiere **alle** Zeilen einer Tabelle
2. Aktualisiere einen Teil der Datensätze mit Hilfe von ***WHERE*** und ***OR***/***AND***
3. Aktualisiere mehrere Felder gleichzeitig
4. Setze mindestens ein Feld auf ***NULL***



Natürlich darfst du auch andere / weitere Statements schreiben



## Auftrag 4 - Daten löschen

Mit dem ***DELETE***-Befehl können wir Daten löschen. Teste dies mit den folgenden Bedingungen:

1. Lösche einen Teil der Datensätze mit Hilfe von ***WHERE*** und ***OR***/***AND***
2. Lösche alle Inhalte aller Tabellen (nützlich zum zurücksetzen der Daten für weitere Tests)

