# Optimierungen / Administrierung

[[_TOC_]]

## Execution Plan

Die Sprache SQL ist sehr mächtig. Das DBMS untersucht und zerlegt diesen Befehl (diesen Vorgang nennt man "parsen"). Basierend auf dieser Untersuchung erstellt es einen Plan für sein Vorgehen, den sogenannten "Zugriffspfad" (Query Execution Plan).

Ein Optimizer ist eine interne Softwarekomponente, die für die Verbesserung der Ausführung von SQL-Abfragen verantwortlich ist. Seine Hauptaufgabe ist es, den effizientesten Weg (=Zugriffspfad) zu finden, um eine Anfrage auszuführen. Dazu bewertet er verschiedene mögliche Ausführungspläne und wählt den besten aus.

Um dem Optimizer bei der Arbeit zuzuschauen und zu verstehen, wie er die Ausführung von SQL-Abfragen plant, können verschiedene Befehle und Werkzeuge verwendet werden, die auf das Anzeigen von Ausführungsplänen und Leistungsstatistiken abzielen. 

### MySQL

**EXPLAIN**: Dieser Befehl wird vor einer **SELECT**, **INSERT**, **DELETE**, **REPLACE**, und **UPDATE** Abfrage verwendet, um den Ausführungsplan für diese Abfrage anzuzeigen. Zum Beispiel:

``` sql
EXPLAIN SELECT * FROM Tabelle WHERE Spalte = 'Wert';
```

**EXPLAIN ANALYZE (MySQL 8.0.18 und höher)**: Gibt detailliertere Informationen über den Ausführungsplan zurück, einschließlich der tatsächlichen Ausführungszeit der verschiedenen Operationen.

### PostgreSQL

**EXPLAIN**: Zeigt den Ausführungsplan für eine Abfrage.

```sql
EXPLAIN SELECT * FROM Tabelle WHERE Spalte = 'Wert';
```

**EXPLAIN ANALYZE**: Führt die Abfrage tatsächlich aus und liefert neben dem Ausführungsplan auch Statistiken zur Laufzeit.

```sql
EXPLAIN ANALYZE SELECT * FROM Tabelle WHERE Spalte = 'Wert';
```

### SQL Server

**SET SHOWPLAN_ALL** oder **SET SHOWPLAN_XML ON**: Diese Befehle zeigen den Ausführungsplan für eine Abfrage, ohne sie tatsächlich auszuführen. **SHOWPLAN_XML** gibt den Plan im XML-Format zurück.

``` sql
SET SHOWPLAN_ALL ON;
GO
SELECT * FROM Tabelle WHERE Spalte = 'Wert';
GO
SET SHOWPLAN_ALL OFF;
```

**EXPLAIN**: In SQL Server Management Studio (SSMS) lässt sich auch der grafische Ausführungsplan anzeigen, indem die Abfrage ausgeführt und dann auf das Symbol für den Ausführungsplan geklickt wird.



## Benchmark 

Bevor Optimierungen an Datenbank oder Statements vorgenommen werden (bspw. neue Indizes erstellen), empfiehlt es sich, einen Benchmark durchzuführen. Man misst also bspw. die Ausführungszeit für ein Statement und dokumentiert den verwendeten Zugriffspfad.

Es ist wichtig zu beachten, dass die Ausführungszeit von SQL-Statements von vielen Faktoren beeinflusst wird, einschließlich der Systemlast, der Netzwerklatenz (bei entfernten Datenbankservern), Caching-Effekten und der spezifischen Datenbankkonfiguration. Aus diesem Grund ist es ratsam, Messungen unter möglichst kontrollierten Bedingungen durchzuführen, um konsistente und aussagekräftige Ergebnisse zu erhalten.

Die einfachste Methode, um die Ausführungszeit eines SQL-Statements zu messen, hängt vom verwendeten Datenbanksystem und den zur Verfügung stehenden Werkzeugen ab. Hier sind einige allgemeine Ansätze für verschiedene Umgebungen:

### MySQL

In MySQL können Sie den Befehl SHOW PROFILES; verwenden, um die Ausführungszeit von Queries zu messen. Zuerst müssen Sie das Profiling aktivieren:

``` sql
SET profiling = 1;
```

Führen Sie dann Ihr SQL-Statement aus:

``` sql
SELECT * FROM IhreTabelle;
```

Um die Ausführungszeit zu sehen, verwenden Sie:

``` sql
SHOW PROFILES;
```

Dies zeigt eine Liste der zuletzt ausgeführten Statements inklusive ihrer Ausführungszeiten an.

### PostgreSQL

In PostgreSQL können Sie den EXPLAIN ANALYZE Befehl verwenden, um nicht nur den Ausführungsplan eines Statements zu sehen, sondern auch, wie lange jede Operation gedauert hat:

``` sql
EXPLAIN ANALYZE SELECT * FROM IhreTabelle;
```

Dies gibt detaillierte Informationen über die Query-Ausführung zurück, einschließlich der Gesamtausführungszeit.

### SQL Server

Im SQL Server Management Studio (SSMS) können Sie einfach die Option "Client Statistics" aktivieren, bevor Sie Ihr Query ausführen. Dies zeigt verschiedene Statistiken nach der Ausführung des Queries an, einschließlich der Ausführungszeit. Um diese Option zu aktivieren, können Sie:

Im Menü "Query" die Option "Include Client Statistics" auswählen.
Ihr SQL-Statement ausführen.
Die Registerkarte "Client Statistics" überprüfen, um die Ausführungszeit zu sehen.

### Client-Tools

Viele SQL-Client-Tools und integrierte Entwicklungsumgebungen (IDEs) bieten eigene Mechanismen zur Messung der Ausführungszeit von SQL-Statements, oft ohne dass zusätzlicher SQL-Code erforderlich ist. Überprüfen Sie die Dokumentation Ihres spezifischen Tools, um zu sehen, ob und wie es die Ausführungszeit direkt messen kann.



## Indizes (Indexe)

Stell dir vor, ein Index in einer Datenbank ist wie das Inhaltsverzeichnis in einem Buch. Wenn du ohne Inhaltsverzeichnis nach einem bestimmten Kapitel suchen würdest, müsstest du möglicherweise jede einzelne Seite durchblättern, bis du findest, was du suchst. Das kann viel Zeit kosten, besonders bei einem dicken Buch.

Ein Inhaltsverzeichnis listet jedoch alle Kapitel mit den entsprechenden Seitenzahlen auf. Wenn du also nach einem bestimmten Thema suchst, kannst du einfach ins Inhaltsverzeichnis schauen, die Seitenzahl finden und direkt zu der Seite gehen, ohne alles andere durchsuchen zu müssen.

In der Datenbank hilft ein Index ähnlich, Daten schnell zu finden. Ohne Index muss die Datenbank jede Zeile in einer Tabelle durchgehen ("durchblättern"), um zu finden, was du suchst. Mit einem Index kann die Datenbank direkt zu den Daten springen, die du brauchst, ähnlich wie du direkt zu einem Kapitel in einem Buch springen kannst, wenn du das Inhaltsverzeichnis benutzt. Das macht das Suchen und Arbeiten mit Daten viel schneller und effizienter.

**Performance**

Indexe können die Geschwindigkeit von Datenabfragen erheblich verbessern, indem sie den Suchaufwand reduzieren. Sie ermöglichen der Datenbank, den Ort der gesuchten Daten schnell zu finden, anstatt jede Zeile in der Tabelle zu durchsuchen. **Aber Achtung**: Ein Index macht zwar die Suche schneller, aber das Einfügen von Daten langsamer, weil beim Einfügen von Daten der Index gepflegt werden muss. 

**Auswahl der zu indizierenden Spalten**

Nicht jede Spalte sollte indiziert werden. Spalten, die häufig in WHERE-Klauseln, JOIN-Bedingungen oder als Teil von ORDER BY und GROUP BY verwendet werden, sind gute Kandidaten für Indexe. Selten verwendete Spalten oder solche mit einer hohen Anzahl von Duplikaten sind weniger geeignet.

**Datenstrukturen**

Verschiedene Datenbanksysteme bieten unterschiedliche Typen von Indexstrukturen an, wie B-Bäume, Hash-Indexe, volltextsuche Indexe und räumliche Indexe. Jeder Typ hat spezifische Anwendungsfälle und Leistungseigenschaften.

**Literatur**

- [Tutorialspoint: B Trees](https://www.tutorialspoint.com/data_structures_algorithms/b_trees.htm)
- [TBZ Grundbildung: Datenstrukturen](https://gitlab.com/ch-tbz-it/Stud/m162/-/blob/main/Daten_Formate/Datenstrukturen.md?ref_type=heads)
- [Wikipedia: B-Baum](https://de.wikipedia.org/wiki/B-Baum)
- [B-Bäume als Datenbank-Index](https://tirsus.com/b-baeume-datenbank-index/)



## Gründe für schlechte Performance 

Im Folgenden werden mögliche Gründe aufgeführt, welche zu übermässigen Laufzeiten bei Zugriffen führen können.

**Locks/Blockierungen**

Wenn mehrere Transaktionen gleichzeitig auf dieselben Daten zugreifen, können Locks entstehen, die andere Operationen blockieren, bis die ursprüngliche Transaktion abgeschlossen ist. Dies kann besonders in hochkonkurrierenden Umgebungen zu Verzögerungen führen. 

**Suboptimale Abfragegestaltung**

Ineffiziente SQL-Abfragen, die unnötige Spalten abrufen, komplexe Berechnungen in der WHERE-Klausel verwenden oder unnötig verschachtelte Subqueries enthalten, können die Ausführungszeit erhöhen.

- Verwendung von Funktionen oder Berechnungen: Die Verwendung von Funktionen (insbesondere benutzerdefinierten Funktionen) oder komplexen Berechnungen in Abfragen kann die Ausführung verlangsamen, da diese für jede Zeile ausgeführt werden müssen.
- Komplexe Joins: Die Verwendung von mehreren Joins oder Joins über grosse Tabellen kann die Leistung beeinträchtigen, insbesondere wenn die Join-Bedingungen nicht effizient sind oder wenn keine geeigneten Indizes vorhanden sind.

**Fehlende, ineffiziente oder zu viele Indizes (Indexe)**

Wie bereits erklärt, haben die Indexe einen grossen Einfluss auf die Performance.

**Datenbankkonfiguration und -optimierung**

Eine nicht optimale Konfiguration der Datenbank oder des Datenbankservers, wie z.B. falsch eingestellte Puffergrössen oder Cache-Einstellungen, kann die Leistung beeinträchtigen.



## Autorisierung / Authentifizierung

Die Befehle um Benutzer zu erstellen und Rechte zu vergeben sind nicht standardisiert und können pro RDBMS abweichen. Grundsätzlich kannst du aber davon ausgehen, dass die folgenden Befehle vorhanden sind (mit dann unterschiedlichen Optionen).

**Beispiele**

```sql
-- Benutzer erstellen
CREATE USER 
-- Benutzer löschen
DROP USER
-- Rechte vergeben
GRANT
-- Rechte nehmen
REVOKE

-- Mysql/Mariadb: Beispiele 
CREATE USER user1@localhost IDENTIFIED BY 'abcd'; 
-- dieser Befehl erstellt einen Benutzer mit Benutzername 'user1' und Passwort 'abcd'. Dieser Benutzer kann aber nur von der gleichen Maschine einloggen auf der auch die Datenbank-Instanz läuft
CREATE USER user2@% IDENTIFIED BY 'abcd'; 
-- Dieser Bentuzer kann auch von anderen Maschinen (Remote) einloggen auf die Datenbank.
GRANT SELECT, INSERT, DELETE, UPDATE ON mydatabase TO 'user3'@'localhost';
-- Dem Benutzer 'user3'@'localhost' werden Rechte für die 4 genannten Befehle gegeben auf die Datenbank 'mydatabase'
GRANT ALL ON *.* TO 'admin'@'%' IDENTIFIED BY 'password' WITH GRANT OPTION;
-- Hier wird der Benutzer 'admin' mit Passwort 'password' erstellt, wobei er gleich alle Rechte (ALL) auf alle Datenbanken (*.*) bekommt. Zusätzlich beommt der das Recht selbst Rechte zu vergben (WITH GRANT OPTION)

```

**Tipps:**

- Erstellt ausreichend Benutzer und teilt keine Benutzer - speziell nicht zwischen Applikationen. Jede Applikation, jede Person soll eigene Benutzer bekommen
- Verwendet das Least Privilege Prinzip. Also nur minimale Rechte vergeben.
- Für eine Applikation überlegt euch, welche Operationen zu welchem Zeitpunkt notwendig sind. So kann z.b. für Lese-Operationen ein anderer Benutzer verwendet werden als für Schreib-Operationen. So vermeidet man das Risiko für SQL-Injection Angriffe.



## Backup

Backup und Recovery sollte im Rahmen der IT Umgebung angeschaut werden. Oft werden Datenbank-Services verwendet (PAAS oder SAAS). Dort kann man das Backup einfach konfigurieren. Bei eigenen Cloud-Instanzen kann auf die Erstellung von Snapshots zurückgegriffen werden. 

Falls die Cloud-Varianten nicht ausreichen, haben alle RDBMS eigene Backup-Tools, die man verwenden kann:

[MySql](https://dev.mysql.com/doc/refman/8.4/en/backup-and-recovery.html)

[MariaDb](https://mariadb.com/kb/en/backup-and-restore-overview/)

[Postgres](https://www.postgresql.org/docs/current/backup.html)

[SQL Server](https://learn.microsoft.com/en-us/sql/relational-databases/backup-restore/create-a-full-database-backup-sql-server?view=sql-server-ver16)