# Logisches und Physisches Modell - Assignments

[[_TOC_]]

## Auftrag 1

Installiere eine lokale Datenbank mit Docker. Wir werden im Unterricht mit [mariaDB](../RDBMS/Readme.md) arbeiten, ihr könnt euch aber auch für andere Datenbanktypen entscheiden.



## Auftrag 2

Mache dich mit MySql Workbench bekannt. Es gibt dazu einen [Auftrag aus der Grundbildung](https://gitlab.com/ch-tbz-it/Stud/m162/-/blob/main/Datenmodellierung/Auftr%C3%A4ge/Auftrag_MySqlWorkbench.md?ref_type=heads), der dir helfen wird und Schritt für Schritt Anleitungen hat.

Es gibt auch [andere Tools](../Tools/Readme.md) (auch eigene), die ihr einsetzen könnt und auch dürft. 



## Auftrag 3

Wandel dein konzeptionelles Diagramm für **dein Thema** welches du gewählt hast, in ein logisches, resp physisches Modell um. Wende die Schritte aus der Theorie an. 

Das Resultat dieses Auftrags ist eine Modell-Datei aus MySql Workbench. Es muss noch keine Datenbank existieren, nur das Modell.

**Wichtig**: Stellt sicher, dass die folgenden Datentypen alle verwendet werden: varchar(xx), int, double, boolean/bit, datetime


