# Datenmodellierung - Assignments

[[_TOC_]]



## Auftrag 1

Führe die [Übung aus der Grundbildung](https://gitlab.com/ch-tbz-it/Stud/m162/-/blob/main/Datenmodellierung/Auftr%C3%A4ge/Auftrag_Kardinalitaeten.md) durch und weise die entsprechenden Kardinalitäten zu.



## Auftrag 2

Erstellt ein konzeptionelles Diagramm von euerer Firma. Bildet die Struktur der Abteilungen und/oder Projektteams ab. Zeigt die hierarchische Struktur (Vorgesetzte, etc). Fügt dann noch mögliche Kunden oder Stakeholder hinzu. 

Zum konzeptionellen Diagramm gehören Entitäten (noch ohne Attribute), Beziehungen und Kardinalitäten. 

Ihr könnt in 2er oder 3er Gruppen arbeiten



## Auftrag 3

Erstelle ein konzeptionelles Diagramm vom Zürcher Tramnetz. Beachtet dabei Tramlinien, Tramzüge, Fahrplan- und Zeiten, Fahrer, Haltestellen und evtl. sogar Kunden und Tickets. 

Ihr könnt in 2er oder 3er Gruppen arbeiten



## Auftrag 4 (Pflicht)

Erstellt nun ein konzeptionelles Diagramm von einem eigenen Thema. Das Thema soll für euch persönlich relevant sein, z.B. ein Hobby (Sportklub, Filmdatenbank, Gamestore, etc). Dieses Thema werdet ihr dann auch länger verfolgen und später auch mit NoSql-Datenbanken abbilden.

Damit ihr später sinnvolle Abfragen erstellen könnt, solltet ihr mindestens 2 many-to-many Beziehungen haben und mindestens 6 Entitäten involvieren.