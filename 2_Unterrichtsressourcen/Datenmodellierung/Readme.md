# Datenmodellierung

[[_TOC_]]

## Einführung

Viele der Konzepte werden in der Grundbildung von der TBZ bereits definiert. Wir verweisen daher oft auf die Unterlagen der Grundbildung und erweitern - wenn notwendig - die Inhalte. 

## Begriffe

* **Entity-Relationship-Modell (ERM) / Entity-Relationship-Diagramm (ERD)**: Ein konzeptionelles Modellierungswerkzeug, das Entitäten (Datenobjekte) und deren Beziehungen untereinander darstellt.
* **Entität**: Ein eindeutig identifizierbares Objekt oder Konzept in der realen Welt, das in der Datenbank gespeichert wird.
* **Attribut**: Eine Eigenschaft oder ein Merkmal einer Entität, das Werte speichert.
* **Relation (Tabelle)**: Eine Sammlung von Daten, organisiert in Zeilen (Datensätzen) und Spalten (Attributen), die eine Entität oder eine Beziehung repräsentiert.
* **Schlüssel (Key)**:
  * **Primärschlüssel (Primary Key, PK)**: Ein Attribut oder eine Kombination von Attributen, das/die jeden Datensatz in einer Tabelle eindeutig identifiziert.
  * **Fremdschlüssel (Foreign Key, FK)**: Ein Attribut in einer Tabelle, das auf den Primärschlüssel einer anderen Tabelle verweist, um Beziehungen zwischen Tabellen zu etablieren.
* **Beziehung (Relationship)**: Beschreibt, wie zwei oder mehr Entitäten miteinander in Verbindung stehen. Beziehungen können unterschiedliche Formen annehmen, wie 1:1, 1:n oder n:m, abhängig davon, wie Entitäten zueinander in Bezug gesetzt werden.
* **Kardinalität**: Gibt die Anzahl der Elemente in der einen Entität an, die einer Anzahl von Elementen in einer anderen Entität zugeordnet sein können, und definiert somit die Art der Beziehung zwischen den Entitäten (z.B. eins-zu-eins, eins-zu-viele, viele-zu-viele).
* **Krähenfussnotation**: Eine grafische Darstellungsmethode innerhalb von Entity-Relationship-Diagrammen (ER-Diagrammen), die dazu dient, die Beziehungen zwischen Entitäten zu illustrieren. Die Krähenfussnotation verwendet Symbole am Ende der Verbindungslinien, die aussehen wie die Zehen eines Vogels (daher der Name), um die Kardinalität der Beziehung zu beschreiben – z.B. ein einfacher Strich für 'eins', ein Krähenfuss für 'viele' und ein Kreis für 'optional'.


### Kompetenzen

* B11.7 Datenbanken aufgrund konzeptioneller Datenmodelle logisch abbilden und in Applikationen integrieren (Niveau: 3)

### Lernziele / Taxonomie

* **Grundlegende Konzepte verstehen**: Teilnehmer können die grundlegenden Konzepte und Terminologien der Datenmodellierung, einschliesslich Entitäten, Attribute, Beziehungen, Schlüssel (Primär- und Fremdschlüssel), Normalisierung erklären.
* **Entitäten und Beziehungen identifizieren**: Lernende sind in der Lage, in gegebenen Szenarien oder Anforderungen relevante Entitäten zu identifizieren und die Beziehungen zwischen diesen Entitäten korrekt zu definieren, einschliesslich der Festlegung von Kardinalitäten.
* **Entity-Relationship-Diagramme (ERDs) erstellen**: Teilnehmer können Entity-Relationship-Diagramme (ERDs) unter Verwendung der Krähenfussnotation entwerfen, um die Struktur der Datenbank und die Beziehungen zwischen den Entitäten visuell darzustellen.



## ERD und ERM

[Link auf Theorie](https://gitlab.com/ch-tbz-it/Stud/m162/-/blob/main/Datenmodellierung/Theorie_Datenmodellierung.md?ref_type=heads#erd-und-erm)



## Konzeptionelles Diagramm

[Assoziationen (Beziehungen) und Kardinalitäten](https://gitlab.com/ch-tbz-it/Stud/m162/-/blob/main/Datenmodellierung/Theorie_Datenmodellierung.md?ref_type=heads#assoziationen-beziehungen-und-kardinalit%C3%A4ten)

**Erweiterung der Syntax mit Krähenfuss**

| Lehner Syntax | Erklärung                                                    | Krähenfuss Syntax                   |
| ------------- | ------------------------------------------------------------ | ----------------------------------- |
| 1             | Genau ein Datensatz                                          | ![krähenfuss_1](krähenfuss_1.png)   |
| c             | Kein oder genau ein Datensatz                                | ![krähenfuss_c](krähenfuss_c.png)   |
| m             | Mindestens ein Datensatz. Kann auch unendlich viele haben.   | ![krähenfuss_m](krähenfuss_m.png)   |
| mc            | Beliebig viele Datensätze. Also kein, ein oder unendlich viele | ![krähenfuss_mc](krähenfuss_mc.png) |

**Beziehungstypen Umgangssprache**

In der Praxis spricht man grundsätzlich von zwei Beziehungstypen, die zur Erstellung von Entitäten relevant sind:

**One-To-Many oder 1:N**: Das sind alle Beziehungen mit 1:m, c:m, 1:mc, c:mc (also auf einer Seite eine Mehrfachbeziehung). Auch 1:1, 1:c, c:c Beziehungen fallen in diese Kategorie, aber diese Beziehungen existieren eher selten

**Many-To-Many oder N:N**: Das sind alle Beziehungen mit m:m, mc:m, mc:mc (also auf beiden Seiten eine Mehrfachbeziehung). DIese Beziehungen sind die netzwerkförmigen Beziehungen.

[Aufträge](./Aufträge_KonzModell.md)



## Redundanzen

[Link auf Theorie](https://gitlab.com/ch-tbz-it/Stud/m162/-/blob/main/Datenmodellierung/Theorie_Datenmodellierung.md?ref_type=heads#redundanzen)



## Konzeptionelles, Logisches und Physisches Datenmodell

[Link auf Theorie](https://gitlab.com/ch-tbz-it/Stud/m162/-/blob/main/Datenmodellierung/Theorie_Datenmodellierung.md?ref_type=heads#konzeptionelles-logisches-und-physisches-datenmodell)

[Aufträge](./Aufträge_LogischesModell.md)



## Normalformen

[Link auf Theorie](https://gitlab.com/ch-tbz-it/Stud/m162/-/blob/main/Datenmodellierung/Theorie_Normalisierung.md)

[Aufträge](./Aufträge_Normalisierung.md)



## Datenkonsistenz und Integrität

[Link auf Theorie](https://gitlab.com/ch-tbz-it/Stud/m162/-/blob/main/Datenmodellierung/Theorie_Datenmodellierung.md?ref_type=heads#datenkonsistenz-und--integrit%C3%A4t)