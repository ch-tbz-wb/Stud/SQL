# Normalisierung - Assignments

[[_TOC_]]

## Auftrag 1

Verwende die Tabelle aus der [Übung der Grundbildung](https://gitlab.com/ch-tbz-it/Stud/m162/-/blob/main/Datenmodellierung/Auftr%C3%A4ge/Auftrag_School_I.md?ref_type=heads) und erstelle direkt ein normalisiertes ERD **in der 3. Normalform**.

Erstelle das Diagramm direkt in MySql Workbench. Falls es dir hilft zuerst ein konzeptionelles Diagramm zu erstellen, kannst du natürlich gerne diesen Zwischenschritt durchführen.



## Auftrag 2

Man kann auch ein Modell ableiten aufgrund von bestehenden Tabellen. Dies wäre der umgekehrte Weg. Teste dein Verständnis und führe die [Übung der Grundbildung](https://gitlab.com/ch-tbz-it/Stud/m162/-/blob/main/Datenmodellierung/Auftr%C3%A4ge/Auftrag_Arbeitsumgebung_I.md?ref_type=heads) durch.



## Auftrag 3

Es existieren ein paar weitere Aufträge, die du gerne noch ausführen kannst. Nimm einfach die Übungen, von denen du denkst, du profitierst am Meisten. Die Übungen haben unterschiedliche Schwierigkeitsstufen.

[Crypto](https://gitlab.com/ch-tbz-it/Stud/m162/-/blob/main/Datenmodellierung/Auftr%C3%A4ge/Auftrag_Crypto_I.md?ref_type=heads)

[CineVille](https://gitlab.com/ch-tbz-it/Stud/m162/-/blob/main/Datenmodellierung/Auftr%C3%A4ge/Auftrag_Cineville.md?ref_type=heads)

[Reisebüro](https://gitlab.com/ch-tbz-it/Stud/m162/-/blob/main/Datenmodellierung/Auftr%C3%A4ge/Auftrag_Reiseb%C3%BCro.md?ref_type=heads)

[Veloreparatur](https://gitlab.com/ch-tbz-it/Stud/m162/-/blob/main/Datenmodellierung/Auftr%C3%A4ge/Auftrag_Veloreparatur.md?ref_type=heads)

[Velotour](https://gitlab.com/ch-tbz-it/Stud/m162/-/blob/main/Datenmodellierung/Auftr%C3%A4ge/Auftrag_Velotour.md?ref_type=heads)



