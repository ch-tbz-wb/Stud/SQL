# Tools für die Arbeit mit RDBMS

[[_TOC_]]

## Tools für die konzeptionelle Arbeit

* [dbdiagram.io](https://dbdiagram.io/home)
* ERD erstellen [DrawIO](https://app.diagrams.net/)
* MySQL Workbench [MySQL Workbench](https://dev.mysql.com/downloads/workbench/)


## GUI-Tools für die Verwaltung

### MySQL Workbench

Die _MySQL Workbench_ ermöglicht zwar nur den Zugriff auf MySQL-Datenbanken, dafür bietet es für die Modellierung sehr gute Unterstützung. Sogar Reverse Engineering ist möglich (aus Datenbanken die Modelle generieren).

[Download MySQL Workbench](https://dev.mysql.com/downloads/workbench/)

### HeidiSQL

Das Programm _HeidiSQL_ erlaubt die Arbeit mit verschiedenen DBMS (MySQL, MariaDB, MS SQL Server, PostgreSQL, SQLite).

[Download HeidiSQL](https://www.heidisql.com/download.php)

### phpMyAdmin

Eine sehr einfache Art für den Einstieg in MariaDB bietet die XAMP-Entwicklungsumgebung. Mit einer einfachen Installation erhält man Webserver (Apache), DBMS (MariaDB), Programmierumgebung (PHP) und eben eine Administrationsumgebung für die DB (phpMyAdmin).

[Download XAMPP](https://www.apachefriends.org/de/)

### Adminer

_Adminer_ (eine Weiterentwicklung von phpMyAdmin) erlaubt die Arbeit mit verschiedenen DBMS: MySQL, MariaDB, PostgreSQL, SQLite, MS SQL Server, Oracle, Elasticsearch und sogar MongoDB. Adminer muss im Zusammenhang mit einem Webserver eingesetzt werden.

[Download Adminer](https://www.adminer.org/) 
[Adminer unter Docker](https://hub.docker.com/_/adminer/)

## Tools für Monitoring

* [IDERA](https://www.idera.com/productssolutions/freetools/?utm_source=sqleb&utm_medium=email&utm_campaign=sql_sv&utm_content=160610-sv) bietet verschiedene Gratis-Tools an.

