# Relationale Datenbankmanagement-Systeme (RDBMS)

[[_TOC_]]

*Auf dem Markt gibt es eine grosse Anzahl an Anbietern für Relationale Datenbankmanagement-Systeme. In diesem Teil geht es darum, zu definieren, was alle RDBMS auszeichnet und einige verbreitete Produkte kennenzulernen.*

## Eigenschaften von RDBMS

Alle RDBMS zeichnen sich durch folgende Eigenschaften aus:

* **Modell**: Sie nutzen das Relationenmodell, was bedeutet, dass alle Daten und Beziehungen in Tabellenform organisiert sind. So lassen sich Zusammenhänge und Wiederholungen erkennen.
* **Schema**: Die Struktur der Datenbank, inklusive Tabellen, Schlüssel und Integritätsregeln, wird im relationalen Schema festgehalten.
* **Sprache**: SQL wird verwendet, um Daten zu definieren, auszuwählen und zu ändern. Diese Sprache ist beschreibend.
* **Architektur**: Die Architektur ermöglicht es, Daten und Programme getrennt zu halten. Änderungen an der Datenbankstruktur sind ohne Anpassung der Programme möglich.
* **Mehrbenutzerbetrieb**: Mehrere Benutzer können gleichzeitig auf die Datenbank zugreifen und sie bearbeiten, ohne sich gegenseitig zu stören. Das System sorgt für korrekte Daten.
* **Konsistenzgewährung**: Es gibt Hilfsmittel, um sicherzustellen, dass die Daten richtig und fehlerfrei gespeichert werden.
* **Datensicherheit und Datenschutz**: Es gibt Schutzmechanismen gegen Datenzerstörung, -verlust und unbefugten Zugriff.

NoSQL-Datenbanksysteme erfüllen obige Eigenschaften nicht vollständig. Aus diesem Grund sind RDBMS in den meisten Firmen verbreitet. Bei massiv verteilten Anwendungen im Web oder bei "Big Data"-Anwendungen muss die relationale Datenbanktechnologie mit NoSQL-Technologien ergänzt werden, um Webdienste rund um die Uhr anbieten zu können.

## Produkte

| Produkt                | Lizenzierung/Besonderheiten                                                                                                |
| :--------------------- | :------------------------------------------------------------------------------------------------------------------------- |
| [Oracle XE]            | kostenlose Version der Oracle Database (Upgrade-Pfad auf Vollversion )                                                     |
| [MySQL]                | kostenlos (GNU GPL), weit verbreitet, skalierbar, starke Community                                                         |
| [MariaDB]              | Open Source (GNU GPL), ursprünglich Fork von MySQL, Temporal Data Tables (Zustände in Vergangenheit!), Community-getrieben |
| [Microsoft SQL Server] | Kommerzielle Lizenz. Umfangreiche Tools (SSMS, SSDT), OLAP                                                                 |
| [PostgreSQL]           | PostgreSQL-Lizenz (liberale Open-Source-Lizenz, ähnlich der MIT-Lizenz). Multi-Version Concurrency Control (MVCC)          |

### Docker-Installationen

In diesem Teil wird gezeigt, wie einige RDBMS in einem Docker-Container betrieben werden können.

#### MariaDB

`docker run --detach --name {containerName} -p {port}:3306 --env MARIADB_ROOT_PASSWORD=my-secret-pw  mariadb:latest`

#### MySQL

`docker run --name {containerName} -p {port}:3306 -p {portX}:33060 -e MYSQL_ROOT_PASSWORD={passwort} -d mysql:latest`


#### PostgreSQL

`docker run --name {containerName} -p {port}:5432 -e POSTGRES_PASSWORD={passwort} -d postgres:latest`


#### MS SQL Server

`docker run --name mssqlserver --network myNet -e ACCEPT_EULA=Y -e SA_PASSWORD={passwort} -p 1433:1433 -d mcr.microsoft.com/mssql/server:latest`



[Oracle XE]: https://www.oracle.com/database/technologies/xe-downloads.html
[MySQL]: https://dev.mysql.com/downloads/installer/
[MariaDB]: https://mariadb.org/download/?t=mariadb&p=mariadb&r=11.3.2&os=windows&cpu=x86_64&pkg=msi&m=mva
[Microsoft SQL Server]: https://www.microsoft.com/de-ch/sql-server/sql-server-downloads
[PostgreSQL]: https://www.postgresql.org/download/



## Tools

Eine (nicht abschliessende) Liste von zusätzlichen anderen Tools [findet ihr hier](../Tools/Readme.md)