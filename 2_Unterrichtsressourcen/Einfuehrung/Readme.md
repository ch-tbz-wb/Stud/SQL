# Einführung in relationale Datenbanken

[[_TOC_]]

## Lernziele

* Sie kennen die wichtigsten Begriffe im Zusammenhang mit relationalen Datenbanksystemen

## Wichtige Begriffe

* **Daten**: Rohfakten oder Zahlen, die für sich allein genommen keine direkte Bedeutung haben.
* **Datenbank**: Eine strukturierte Sammlung von Daten, die elektronisch gespeichert ist.
* **Datenbankmanagementsystem (DBMS)**: Software zur Erstellung, Verwaltung und Manipulation von Datenbanken.
* **Wissenstreppe**: Entstehung von Wissen nach [Klaus North](https://de.m.wikipedia.org/wiki/Klaus_North).
* **Relationale Datenbank**: Eine Datenbank, die auf dem relationalen Modell basiert, bei dem Daten in Tabellen organisiert sind.

## Die Wissenstreppe

Der Unterschied zwischen Daten und Informationen ist ein grundlegendes Konzept in der Informatik, der Datenverarbeitung und im Informationsmanagement. Obwohl diese Begriffe oft synonym verwendet werden, bezeichnen sie unterschiedliche Konzepte:

![wissenstreppe](wissenstreppe.png)

### Daten

* **Definition**: Daten sind Rohfakten oder Zahlen, die für sich allein genommen keine direkte Bedeutung haben. Sie können in unstrukturierter oder strukturierter Form vorliegen und sind die Grundbausteine, aus denen Informationen generiert werden.
* **Eigenschaften**: Daten sind objektiv und kontextlos. Sie bestehen aus Zahlen, Zeichen, Bildern oder anderen Methoden der Aufzeichnung, die in einem Rohzustand existieren.
* **Beispiel**: Eine Liste von Temperaturmesswerten über eine Woche, ungeordnete Kundentransaktionszahlen, oder die Rohpixel eines digitalen Fotos.
### Informationen

* **Definition**: Informationen entstehen, wenn Daten in einem Kontext verarbeitet, organisiert oder strukturiert werden, um sie verständlich und nützlich zu machen. Informationen sind Daten, die auf eine Weise interpretiert, aggregiert oder analysiert wurden, dass sie Bedeutung oder Wert für den Empfänger haben.
* **Eigenschaften**: Informationen sind kontextbezogen und subjektiv, da ihre Bedeutung von der Art und Weise abhängt, wie die Daten interpretiert werden. Informationen sind das Ergebnis der Verarbeitung von Daten, um spezifische Fragen zu beantworten oder Probleme zu lösen.
* **Beispiel**: Ein Wetterbericht, der auf der Grundlage von Temperaturdaten erstellt wurde, um die Wetterbedingungen für die kommende Woche vorherzusagen; eine finanzielle Zusammenfassung, die aus Kundentransaktionsdaten generiert wurde, um die monatlichen Ausgaben zu zeigen.

### Schlüsselunterschiede

* **Verarbeitung**: Daten werden zu Informationen, indem sie verarbeitet und in einen sinnvollen Kontext gestellt werden. Daten sind der Input, während Informationen der verarbeitete und interpretierte Output sind.
* **Bedeutung**: Daten allein bieten keine Schlussfolgerungen, während Informationen aus Daten abgeleitet werden und spezifische Bedeutungen oder Einsichten liefern.
* **Entscheidungsfindung**: Informationen, nicht Daten, sind die Basis für Entscheidungsfindungen. Informationen ermöglichen es Benutzern, auf der Grundlage verarbeiteter und interpretierter Daten fundierte Entscheidungen zu treffen.

### Links
* [Video 2'38''](https://www.youtube.com/watch?v=6N4bwFsjayc) Was ist der Unterschied zwischen Daten, Informationen und Wissen

## Grundlagen zu RDBMS

Aus Tabellenkalkulationsprogrammen (Excel, Google Spreadsheet, etc.) kennen wir alle das Konzept von Tabellen. Die Tabellen bestehen aus Zeilen und Spalten. Der Schnittpunkt zwischen einer Zeile und einer Spalte wird als "Zelle" bezeichnet.

In der obersten Zeile werden gewöhnlich Headerdaten erfasst. Für jede Spalte wird dabei angegeben, um welche Daten es sich bei den Einträgen handelt. Den Zellen können Datentypen zugeordnet werden. 

Beispiel einer einfachen Tabelle:

| Vorname | Nachname | Geburtsdatum |
| ------- | -------- | ----- |
| Tim     | Wagner   | 20.01.1978    |
| Antonia | Suter    | 10.05.1992    |
| Max     | Weber    | 03.12.1962    |
| Til     | Schwager | 05.08.2005    |
| Kurt    | Kummer   | 09.10.1958    |

Auch in relationalen Datenbanken arbeitet man mit Tabellen. Im Unterschied zu Excel ist der Umgang mit Datentypen aber wesentlich strikter. Jeder Spalte muss ein Name und ein Datentyp zugewiesen werden. Dieser Typ bestimmt danach die zulässigen Werte in den Zellen (den Wertebereich oder die Domäne).

### Vergleich Excel- und Datenbank-Tabelle

| Merkmal                         | Excel                                             | Datenbank                                                                              |
| :------------------------------ | :------------------------------------------------ | :------------------------------------------------------------------------------------- |
| Struktur                        | tabellarisch                                      | tabellarisch                                                                           |
| horizontale Bezeichnung         | Zeile/Datensatz                                   | Zeile/Tupel/Datensatz                                                                  |
| vertikale Bezeichnung           | Spalte                                            | Spalte/Eigenschaft/Attribut/Feld                                                       |
| Header (Spaltennamen)           | fakultativ                                        | obligatorisch                                                                          |
| Datentyp                        | tolerant                                          | strikt                                                                                 |
| Datenmanipulation               | direkte Erfassung in den Zellen                   | Erfassung über SQL-Befehle                                                             |
| Datenintegrität und Beziehungen | keine Unterstützung für referenzielle Beziehungen | starke Unterstützung für die Bewahrung von referenzieller Integrität                   |
| Abfragen                        | einfache Abfragen möglich                         | mächtige Möglichkeiten für Abfrage, Manipulation und Aggregation über mehrere Tabellen |
| Mehrbenutzerfähigkeit           | Eingeschränkt (keine Transaktionen)               | Umfassend (Transaktionen/ACID)                                                         |

## Das DBMS als Warenlager (bzw. Datenlager)

Um ein Datenbanksystem (DBMS) verstehen zu können, schauen wir uns eine Analogie an: Ein riesengrosses Warenlager. Dieses Warenlager ist in verschiedene Bereiche unterteilt (bspw. pro Kunde)

Gehen wir davon aus, dass du keinen direkten Zugang ins Lager hast. Wenn du etwas aus dem Lager haben oder wissen möchtest, musst du dich an die Reception (Verwaltung) wenden. Bei jedem Auftrag (Befehl) prüft diese zuerst, ob du dafür berechtigt bist. Erst danach setzt Sie den Auftrag um.

Die Verwaltung des Warenlagers macht ungefähr das, was ein Datenbankmanagementsystem (DBMS) macht. Es verwaltet natürlich keine Waren, sondern Daten. Alle Zugriffe auf die Daten erfolgen über das DBMS. Die Sprache, in welcher Anfragen ans DBMS gerichtet werden, heisst "SQL".

SQL ist eine standardisierte Sprache, die für die Verwaltung und Manipulation von Datenbanken verwendet wird. Über die Sprache SQL können dem DBMS ganz unterschiedliche Befehle erteilt werden. Die drei Hauptkategorien sind DML, DDL und DCL:

| Bezeichnung                | Abkürzung | Verwendung                                                                               | Beispiele                      | Bemerkung                                                                                                            |
| :-------------------------- | :-------: | :---------------------------------------------------------------------------------------- | :------------------------------ | :-------------------------------------------------------------------------------------------------------------------- |
| Data Manipulation Language |    DML    | Zur Abfrage, Erfassung, Änderung und Löschung von Daten                                  | SELECT, INSERT, UPDATE, DELETE | DML-Befehle sind die am häufigsten verwendeten SQL-Statements, da sie den direkten Umgang mit den Daten ermöglichen. |
| Data Definition Language   |    DDL    | Zur Definition, Änderung und Löschung von Schemata, Tabellen und anderen Datenstrukturen | CREATE, ALTER, DROP, TRUNCATE  | DDL-Befehle beeinflussen die Metadaten einer Datenbank und führen zu einer Veränderung der Datenbankstruktur.        |
| Data Control Language      |    DCL    | Zur Verwaltung von Berechtigungen                                                        | GRANT, REVOKE                  | DCL wird verwendet, um die Sicherheit der Datenbank durch Kontrolle des Zugriffs auf Daten und Datenbankoperationen zu gewährleisten. |

## Links

* [Datenbanken und SQL](https://www.youtube.com/watch?v=fgOiWEGNJ-o&list=PL_pqkvxZ6ho1dn7jRkTfoYBXhw5c9jll0) (Einführung und Datenbankmanagement)
* [Datenbanken - Grundlagen](https://www.youtube.com/watch?v=1jw6GFAV76Y)