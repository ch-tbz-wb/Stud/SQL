# Unterrichtsressourcen

*Überblick über die Unterrichtsressourcen*

* [Einführung](./Einfuehrung/Readme.md)
  * Daten und Informationen
  * Wissenstreppe
  * Grundlagen
* [Datenmodellierung](./Datenmodellierung/Readme.md)
  * Datenmodellierung und Entwurfsprinzipien
  * Entity-Relationship-Modell (ERM)
  * Normalisierung und Denormalisierung: Warum und wie
* [RDBMS](./RDBMS/Readme.md)
  * Architektur und Komponenten eines RDBMS
  * Unterschiede und Vergleiche beliebter RDBMS
* [SQL](./SQL/Readme.md)
  * Grundlagen von SQL: SELECT, INSERT, UPDATE, DELETE
  * Fortgeschrittene SQL-Konzepte: Joins, Subqueries, Aggregatfunktionen
  * Datenbanktransaktionen und -sicherheit: Transaktionsmanagement, ACID-Eigenschaften
* [Optimierung / Administration](Optimierung_Administrierung/)
  * Schema-Design und Optimierungstechniken
  * Indexierung und ihre Auswirkungen auf die Leistung
  * Performance-Monitoring und Tuning
  * Authentifizierung und Autorisierung
  * Backup
* [(in bearbeitung) Programmierung](60_Sicherheit/)
  * Funktionen / Stored Procedures
  * Einbindung in Python
