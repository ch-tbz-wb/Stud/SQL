# Handlungsziele und Handlungssituationen 

Zu jedem einzelnen Handlungsziel aus der Modulidentifikation wird eine realistische, beispielhafte und konkrete Handlungssituation beschrieben, die zeigt, wie die Kompetenzen in der Praxis angewendet werden.

### 1. Modellierung von relationalen Datenbanken

Die Lernenden sollen in der Lage sein, relationale Datenbanken zu modellieren, indem sie geeignete Entitäten, Attribute und Beziehungen identifizieren und ein normiertes Datenmodell erstellen.

**Handlungssituation:** Du arbeitest als Datenbankanalyst in einem Unternehmen, das ein neues Kundenmanagement-System einführen möchte. Dein Team benötigt eine strukturierte Datenbank, um Informationen über Kunden, deren Bestellungen und die angebotenen Produkte zu verwalten. Deine Aufgabe besteht darin, die Anforderungen des Systems zu erfassen und ein relationales Datenmodell zu erstellen. Dabei identifizierst du die notwendigen Entitäten (z. B. Kunden, Bestellungen, Produkte), definierst die relevanten Attribute für jede Entität und modellierst die Beziehungen zwischen den Entitäten, um sicherzustellen, dass das Datenmodell den Prinzipien der Normalisierung entspricht und gleichzeitig die Effizienz und Integrität der Daten gewährleistet.

### 2. Die Sprache SQL lesen und anwenden

Die Lernenden sollen SQL-Abfragen lesen, verstehen und selbstständig anwenden können, um Daten abzurufen und zu manipulieren.

**Handlungssituation:** In deinem Unternehmen gibt es eine Datenbank mit Kundendaten, aus der regelmässig Berichte zu bestimmten Kundengruppen abgerufen werden müssen. Dein Teamleiter beauftragt dich, eine Liste mit Kunden zu erstellen, die bestimmte Filterkriterien erfüllen, und die notwendigen SQL-Abfragen dafür zu entwerfen. Dazu liest du bestehende Abfragen, passt sie an und schreibst neue Abfragen, um gezielte Datenabfragen durchzuführen und den gewünschten Bericht zu erstellen.

### 3. Datenbanken erstellen und verwalten

Die Lernenden sollen in der Lage sein, relationale Datenbanken zu erstellen, zu strukturieren und effektiv zu verwalten.

**Handlungssituation:** Ein mittelständisches Unternehmen hat kürzlich eine neue Datenbank zur Verwaltung seiner Kundendaten implementiert und benötigt Unterstützung bei der regelmäßigen Sicherung und Verwaltung. Du wirst beauftragt, die Datenbank nicht nur zu erstellen, sondern auch ein Backup- und Benutzerverwaltungskonzept zu entwickeln. Dabei richtest du automatische Backups ein, um die Datenintegrität zu gewährleisten, und definierst Benutzerrollen und Zugriffsrechte, damit unterschiedliche Abteilungen kontrollierten Zugriff auf die Datenbank erhalten und Datensicherheit im gesamten Unternehmen sichergestellt ist.

### 4. Datenbanksysteme überwachen und optimieren

Die Lernenden sollen Datenbanksysteme überwachen, Leistungsprobleme identifizieren und Optimierungen vornehmen können, um die Effizienz und Stabilität zu gewährleisten.

**Handlungssituation:** Dein Unternehmen stellt fest, dass die Datenbank für die Buchungsplattform zu Spitzenzeiten langsamer wird, was die Benutzererfahrung beeinträchtigt. Deine Aufgabe ist es, das System auf Leistungsengpässe zu überwachen, Probleme zu identifizieren und Optimierungen vorzunehmen. Du analysierst Abfragezeiten und Serverressourcen, implementierst Indexierungen und optimierst SQL-Abfragen, um die Datenbankperformance und die Stabilität der Plattform auch bei hoher Nutzung sicherzustellen.

