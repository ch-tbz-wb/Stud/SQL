# Handlungskompetenzen

## Kompetenzmatrix

| Kompetenzband                                 | RB           | HZ   | Novizenkompetenz                                             | Fortgeschrittene Kompetenz                                   | Kompetenz professionellen Handelns                           | Kompetenzexpertise                                           |
| --------------------------------------------- | ------------ | ---- | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| a) Modellierung von relationalen Datenbanken  | B11.7        | 1    | Kennt die grundlegenden Konzepte der relationalen Datenbankmodellierung und kann einfache Entitäten und deren Attribute benennen. | Kann ein einfaches relationales Datenmodell erstellen, Entitäten und deren Beziehungen definieren und einfache Normalisierungsprozesse anwenden. | Ist in der Lage, komplexe relationale Datenmodelle zu erstellen, die mehrere Entitäten, komplexe Beziehungen und Normalisierungsstufen berücksichtigen. | Entwickelt umfassende und skalierbare Datenbankmodelle für große und komplexe Systeme und leitet Schulungen zur optimalen Modellierung relationaler Datenbanken für Teams an. |
| b) Die Sprache SQL lesen und anwenden         | B11.7        | 2    | Kennt die grundlegenden SQL-Befehle für CRUD-Operationen (Create, Read, Update, Delete) und kann einfache Abfragen formulieren. | Kann komplexe SQL-Abfragen schreiben, einschließlich JOINs, Filtern, Sortieren, Aggregation und grundlegenden Transaktionen, und diese in eine Programmiersprache integrieren. | Ist in der Lage, SQL innerhalb von Programmiersprachen anzuwenden, um Datenbankoperationen zu automatisieren und Transaktionen zur Sicherstellung von Datenintegrität durchzuführen. | Entwickelt umfassende SQL-Strategien zur Optimierung der Datenbankverwaltung und implementiert komplexe Abfragen für effiziente Datenanalyse und Berichterstattung. |
| c) Datenbanken erstellen und verwalten        | B11.7, B13.1 | 3    | Kennt die grundlegenden Schritte zur Erstellung einer relationalen Datenbank. | Kann eine relationale Datenbank erstellen, Daten importieren und grundlegende Verwaltungsaufgaben durchführen. | Kann ein umfassendes Backup- und Benutzerverwaltungssystem für eine Datenbank einrichten und die Datenbankstruktur an die Anforderungen des Unternehmens anpassen. | Entwickelt maßgeschneiderte Datenbanklösungen, einschließlich Backup-Strategien und Benutzerverwaltung, und optimiert diese für hohe Verfügbarkeit und Leistung in großen Systemen. |
| d) Datenbanksysteme überwachen und optimieren | B11.7, B13.1 | 4    | Kennt die grundlegenden Konzepte zur Überwachung von Datenbanksystemen. | Kann einfache Leistungskennzahlen überwachen und Identifizierung von Problemen in der Datenbankperformance durchführen. | Ist in der Lage, umfassende Überwachungsstrategien zu implementieren, um die Systemleistung zu optimieren und Engpässe proaktiv zu beseitigen. | Entwickelt komplexe Monitoring-Lösungen zur Analyse und Optimierung der Datenbankleistung in großen und dynamischen Umgebungen und leitet Maßnahmen zur kontinuierlichen Verbesserung ein. |



## Modulspezifische Handlungskompetenzen

* B11 Applikationen entwickeln, Programme erstellen und testen
   * B11.7 Datenbanken aufgrund konzeptioneller Datenmodelle logisch abbilden und in Applikationen integrieren (Niveau: 3)
 * B13 Konzepte und Services entwickeln
   * B13.1 Archiv-, Backup-, Restore- und Repair-Konzepte für die Software, Datenbestände und Datenbanken erarbeiten (Niveau: 3)

## Allgemeine Handlungskompetenzen

 * A2 Kommunikation situationsangepasst und wirkungsvoll gestalten (Niveau: 3)
   * A2.3 Quantität und Qualität der Informationen adressatengerecht selektieren und daraus folgend die Art und Form der Information festlegen (Niveau: 3)
    * A2.5 Informations- und Kommunikationstechnologien (ICT) professionell einsetzen und etablieren (Niveau: 3)
    * A2.6 Die branchenspezifischen Fachtermini des Engineerings verwenden und diese adressatengerecht kommunizieren (Niveau: 3)
    * A2.7 Berichte professionell und in einer für die Adressaten verständlichen Weise verfassen (Niveau: 3)

 * A3 Die persönliche Entwicklung reflektieren und aktiv gestalten (Niveau: 3)
   * A3.2 Neues Wissen mit geeigneten Methoden erschliessen und arbeitsplatznahe Weiterbildung realisieren (Niveau: 3)
   * A3.4 Die eigenen digitalen Kompetenzen kontinuierlich weiterentwickeln (Niveau: 3)

## Anforderungsniveau

Das Anforderungsniveau einer Kompetenz ist durch die Komplexität der zu lösenden Problemstellung, die Veränderlichkeit und Unvorhersehbarkeit des Arbeitskontextes und die Verantwortlichkeit im Bereich der Zusammenarbeit und Führung definiert. HF Absolvierende sind generell in der Lage Problemstellungen und Herausforderungen zu analysieren, diese adäquat zu bewerten und mit innovativen Problemlösestrategien zu lösen

### Kompetenzniveau 1: Novizenkompetenz

Erfüllen selbständig fachliche Anforderungen; mehrheitlich wiederkehrende Aufgaben in einem überschaubaren und stabil strukturierten Tätigkeitsgebiet; Arbeit im Team und unter Anleitung.

### Kompetenzniveau 2: fortgeschrittene Kompetenz

Erkennen und analysieren umfassende fachliche Aufgabenstellungen in einem komplexen Arbeitskontext und sich veränderndem Arbeitsbereich; führen teils kleinere Teams; erledigen die Arbeiten selbständig unter Verantwortung einer Drittperson.

