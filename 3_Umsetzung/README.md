# Umsetzung

Bereich: Datenbanken
Semester: 3

## Lektionen

* Präsenzlektionen (Vor Ort): 20
* Präsenzlektionen (Distance Learning): 20
* Selbststudium: 20

## Fahrplan

| Lektionen | Selbststudium | Inhalt                                     | Kompetenzband | Tools                         |
| --------- | ------------- | ------------------------------------------ | ------------- | ----------------------------- |
| 12        | 6             | Modellierung von relationalen Datenbanken  | a)            | MySql Workbench, draw.io      |
| 20        | 10            | Die Sprache SQL lesen und anwenden         | b)            | MySql Workbench, AWS, MariaDB |
| 4         | 2             | Datenbanken erstellen und verwalten        | c)            | MySql Workbench, AWS, MariaDB |
| 4         | 2             | Datenbanksysteme überwachen und optimieren | d)            | MySql Workbench, AWS, MariaDB |



## Lernziele

* Grundlagen zu DBMS und relationalen Datenbanken erlernen
* Die Sprache SQL lesen und anwenden
* Datenbanken erstellen und verwalten
* Datenbanksysteme überwachen und optimieren


## Voraussetzungen

M114 - Codierungs-, Kompressions- und Verschlüsselungsverfahren einsetzen

## Dispensation

* Gemäss Reglement HF Lehrgang
* Nachweis der Inhalte gemäss diesem Modul, bspw. durch Abschlüsse der Module aus der Grundbildung (104,105,106,153,162,164,290).

## Technik

GitLab Account, AWS Academy Umgebung

## Methoden

Self-Learning Plattformen und praktische Laborübungen, Coaching durch Lehrperson

## Schlüsselbegriffe

Relationale Datenbank, SQL (Structured Query Language), Datenmanipulationssprache (DML), Data Definition Language (DDL), Data Control Language (DCL), Primärschlüssel (Primary Key), Fremdschlüssel (Foreign Key), Indexierung, Joins, Datenintegrität, Referentielle Integrität, Transaktion, ACID, Normalisierung, Entität, Tabelle, Relation, Record, Attribut, Feld, Attributwert, Beziehungen, Entity Relationship Diagram (ERD), Entity Relationship Model (ERM)

## Lehr- und Lernformen

Lehrervorträge, Lehrgespräche, Workshop, Gastreferenten, Einzel- und Gruppenarbeiten, Lernfragen, Präsentationen

## Lehrmittel

Viele Inhalte werden in Online-Tutorials gut vermittelt und können im individuellen Tempo durchgearbeitet werden:
* [W3School](https://www.w3schools.com/sql/default.asp) mit Quiz
* [Khan Academy "SQL Basics"](https://www.khanacademy.org/computing/computer-programming/sql/sql-basics)
* [tutorialspoint](https://www.tutorialspoint.com/sql/index.htm)
* [SQL Grundkurs](https://tbzedu.sharepoint.com/:b:/r/sites/campus/students/wb/_read-only/Unterlagen/CNE/SQL/90%20-%20B%C3%BCcher/SQL%20Grundkurs%20-%20Hanser.pdf?csf=1&web=1&e=9yVwOR), Hanser Verlag ([Zusatzmaterial](https://tbzedu.sharepoint.com/:u:/r/sites/campus/students/wb/_read-only/Unterlagen/CNE/SQL/90%20-%20B%C3%BCcher/SQL%20Grundkurs%20-%20Hanser%20-%20Zusatzmaterial.zip?csf=1&web=1&e=B8QiNG))

## Hilfsmittel

* Adams Ralf: SQL, Der Grundkurs für Ausbildung und Praxis, HANSER Verlag, ISBN: 978-3-446-47913-5
