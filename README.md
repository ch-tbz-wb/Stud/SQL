![TBZ Logo](./x_gitressourcen/tbz_logo.png) 

# SQL - Relationale Datenbanken mit SQL

## Kurzbeschreibung des Moduls

In diesem Modul erkunden Sie die wichtigsten Prinzipien relationaler Datenbanken und erlernen die Grundlagen von SQL, einer standardisierten Sprache für die Kommunikation mit einem Datenbanksystem. Sie erhalten Einblicke in die Anwendung und Struktur relationaler Datenbanken, ein fundamentales Wissen, das in vielen Branchen und Berufsfeldern Anwendung findet. Durch praktische SQL-Übungen entwickeln Sie Fähigkeiten im effizienten Datenmanagement, was Ihnen ermöglicht, Daten zu analysieren, zu manipulieren und zu verwalten.

## Angaben zum Transfer der erworbenen Kompetenzen

Wir erarbeiten die Grundlagen und Konzepte gemeinsam. Danach werden Sie weitgehend selbstständig und praktisch arbeiten. Vordefinierte Aufgaben werden durchgeführt und durch die Lehrperson unterstützt.

## Abhängigkeiten und Abgrenzungen

### Parallele Module

Parallel zu diesem Modul besuchen Sie das NoSQL-Modul. Dort erhalten Sie Einblicke in nicht-relationale Datenbanken und werden so die (teilweise grossen) Unterschiede direkt erleben.

## Dispensation

* Gemäss Reglement HF Lehrgang
* Nachweis der Inhalte gemäss diesem Modul, bspw. durch Abschlüsse der Module aus der Grundbildung (104,105,106,153,162,164,290).

## Unterlagen zum Modul

### Organisatorisches

[Organisatorisches](0_Organisatorisches) zur Autorenschaft dieser Dokumente zum Modul

## Handlungskompetenzen

[Handlungskompetenzen](1_Handlungskompetenzen) zu den Handlungszielen 

### Unterrichtsressourcen

[Unterrichtsressourcen](2_Unterrichtsressourcen) von Aufträgen und Inhalten zu den einzelnen Kompetenzen

### Umsetzung

[Umsetzung](3_Umsetzung)

### Fragekatalog

[Fragekatalog](4_Fragekatalog) Allgemein

### Handlungssituationen

[Handlungssituationen](5_Handlungssituationen) mit möglichen Praxissituationen zu den einzelnen Handlungszielen

### Zertifizierungen

Mögliche [Zertifizierungen](8_Zertifizierungen) für dieses Modul

- - -

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/ch/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/3.0/ch/88x31.png" /></a><br />Dieses Werk ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/ch/">Creative Commons Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Schweiz Lizenz</a>.

- - -
